import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Button } from "react-bulma-components";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { Link } from "react-router-dom";
import "bootstrap";

// components
import NavBar from "./components/Navbar";
import Footer from "./components/Footer";
import Login from "./components/Login";
import Register from "./components/Register";
import Booking from "./components/Booking";
import AddBooking from "./components/AddBooking";
import AddBooking2 from "./components/AddBooking2";
import AddVenue from "./components/AddVenue";
import UpdateUser from "./components/UpdateUser";
import UpdateVenue from "./components/UpdateVenue";
import UpdateBooking from "./components/UpdateBooking";
import UpdateUserModal from "./components/UpdateUserModal";
import UpdateVenueModal from "./components/UpdateVenueModal";
import UpdateBookingModal from "./components/UpdateBookingModal";
import Landing from "./components/Landing";
import Venue from "./components/Venue";
import User from "./components/User";
import Test from "./components/Test";
import Contact from "./components/Contact";

const client = new ApolloClient({ uri: "http://localhost:4000/graphql" });

function App() {
  const [email, setEmail] = useState(localStorage.getItem("email"));
  // const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
  const [password, setPassword] = useState(localStorage.getItem("password"));

  const updateSession = () => {
    setEmail(localStorage.getItem("email"));
    // setFirstName(localStorage.getItem("firstName"));
    setPassword(localStorage.getItem("password"));
  };
  const Logout = () => {
    localStorage.clear();
    updateSession();
    return <Redirect to="/login" />;
  };

  const loggedUser = props => {
    // ... {spread operator} retains all the existing props
    // and added a new prop called updateSession
    return <Login {...props} updateSession={updateSession} />;
  };
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        {/*<Route path="/" component={Landing} />*/}
        <NavBar username={email} />
        <Route path="/bookings" component={Booking} />
        <Route path="/add_Booking" component={AddBooking2} />
        <Route path="/add_Venue" component={AddVenue} />
        <Route path="/user/update/:id" component={UpdateUser} />
        {/*<Route path="/user/update/:id" component={UpdateUserModal} />*/}
        <Route path="/booking/update/:id" component={UpdateBooking} />
        {/*<Route path="/booking/update/:id" component={UpdateBookingModal} />*/}
        <Route path="/venue/update/:id" component={UpdateVenue} />
        {/*<Route path="/venue/update/:id" component={UpdateVenueModal} />*/}
        <Route path="/home" component={Test} />
        <Route path="/register" component={Register} />
        <Route path="/venues" component={Venue} />
        <Route path="/users" component={User} />
        <Route path="/login" render={loggedUser} />
        <Route path="/logout" component={Logout} />
        <Route path="/contact_us" component={Contact} />
        <Footer />
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
