import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

import DatePicker from "react-datepicker";

// import
// import { logInMutation } from "../queries/mutations";
// queries
// import { getMembersQuery } from "../queries/queries";

const AddBooking = props => {
	console.log(props);
	// hooks
	const [date, setDate] = useState(new Date());

	useEffect(() => {
		console.log("date: " + date);
	});

	const dateChangeHandler = e => {
		setDate(e.target.value);
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Book A Venue</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form>
								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										Date
									</label>
									<input
										className="control is-fullwidth input"
										type="date"
										dateFormat="h:mm aa"
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										Start Time
									</label>
									<input
										className="control is-fullwidth input"
										type="time"
										dateFormat="h:mm aa"
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										End Time
									</label>
									<input
										className="control is-fullwidth input"
										type="time"
										dateFormat="h:mm aa"
									/>
								</div>

								<div className="field">
									<div className="control">
										<label
											className="label"
											htmlFor="firstName"
										>
											Venue
										</label>
										<div className="select is-fullwidth">
											<select>
												<option disabled selected>
													Select Venue
												</option>
												<option>Gym</option>
												<option>Theatre</option>
											</select>
										</div>
									</div>
								</div>

								<Button type="submit" color="success" fullwidth>
									Book Now!
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose()(AddBooking);
// graphql(getMembersQuery, { name: "getMembersQuery" }),
// graphql(logInMutation, { name: "logInMutation" })
