import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

import DatePicker from "react-datepicker";
// import TimePicker from 'react-time-picker';
import "react-datepicker/dist/react-datepicker.css";

// queries
import { getVenuesQuery, getBookingsQuery } from "../queries/queries";

// import
import { createBookingMutation } from "../queries/mutations";

const AddBooking2 = props => {
	console.log(props);
	const moment = require("moment-timezone");
	const now = moment().tz("Asia/Taipei");
	// hooks
	const [date, setDate] = useState(new Date());
	const [startTime, setStartTime] = useState("");
	// const [startTime, setStartTime] = useState(new Date());
	const [endTime, setEndTime] = useState("");
	// const [endTime, setEndTime] = useState(new Date());
	const [venueId, setVenueId] = useState("");
	const [statusId, setStatusId] = useState("");
	const [userId, setUserId] = useState("");

	useEffect(() => {
		console.log("date: " + date);
	});

	const dateChangeHandler = e => {
		setDate(e.target.value);
	};

	const startTimeChangeHandler = e => {
		setStartTime(e.target.value);
	};

	const endTimeChangeHandler = e => {
		setEndTime(e.target.value);
	};

	const venueChangeHandler = e => {
		setVenueId(e.target.value);
	};

	const statusChangeHandler = e => {
		setStatusId(e.target.value);
	};

	const userChangeHandler = e => {
		setUserId(e.target.value);
	};

	const venueOptions = () => {
		console.log(props);
		let venueData = props.getVenuesQuery ? props.getVenuesQuery : [];
		if (venueData.loading) {
			return <option>Loading Venues...</option>;
		} else {
			return venueData.getVenues.map(venue => {
				return (
					<option key={venue.id} value={venue.id}>
						{venue.name}
					</option>
				);
			});
		}
	};

	const addBooking = e => {
		e.preventDefault();

		let newBooking = {
			date: date,
			startTime: startTime,
			endTime: endTime,
			venueId: venueId,
			statusId: "5de0757ebb8c842536c7eea3",
			/*statusId: "",*/
			userId: "5de7303aca80710de426eb5d"
			/*userId: userId */
		};

		console.log(newBooking);

		props.createBookingMutation({
			variables: newBooking,
			refetchQueries: [
				{
					query: getBookingsQuery
				}
			]
		});

		Swal.fire({
			icon: "success",
			title: "Booking sent",
			position: "top-end"
		});
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Book A Venue</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addBooking}>
								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										Date
									</label>

									<DatePicker
										className="control is-fullwidth input"
										showPopperArrow={false}
										selected={date}
										onChange={date => setDate(date)}
										showDisabledMonthNavigation
										minDate={new Date()}
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="startTime"
									>
										Start Time
									</label>
									<DatePicker
										className="control input"
										selected={startTime}
										onChange={startTime =>
											setStartTime(startTime)
										}
										showTimeSelect
										showTimeSelectOnly
										timeIntervals={30}
										dateFormat="h:mm aa"
										min={startTime}
										value={startTime}
										placeholderText="Choose Start Time"
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="endTime">
										End Time
									</label>
									<DatePicker
										className="control input"
										selected={endTime}
										onChange={endTime =>
											setEndTime(endTime)
										}
										showTimeSelect
										showTimeSelectOnly
										timeIntervals={30}
										dateFormat="h:mm aa"
										min={startTime}
										max={endTime}
										value={endTime}
										placeholderText="Choose End Time"
									/>
								</div>

								<div className="field">
									<div className="control">
										<label
											className="label"
											htmlFor="firstName"
										>
											Venue
										</label>
										<div className="select">
											<select
												onChange={venueChangeHandler}
											>
												<option disabled selected>
													Select Venue
												</option>
												{venueOptions()}
											</select>
										</div>
									</div>
								</div>

								<Button type="submit" color="success" fullwidth>
									Book Now!
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(getBookingsQuery, { name: "getBookingsQuery" }),
	graphql(createBookingMutation, { name: "createBookingMutation" })
)(AddBooking2);
// graphql(getMembersQuery, { name: "getMembersQuery" }),
// graphql(logInMutation, { name: "logInMutation" })
