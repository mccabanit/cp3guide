import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import { getVenuesQuery, getAvailabilitiesQuery } from "../queries/queries";
// mutations
import { createVenueMutation } from "../queries/mutations";
import { toBase64, nodeServer } from "../function.js";

const AddVenue = props => {
	console.log(props);
	// hooks
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [availabilityId, setAvailabilityId] = useState("");
	const [imagePath, setImagePath] = useState("");
	const fileRef = React.createRef();

	console.log(fileRef);

	const nameChangeHandler = e => {
		setName(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const availabilityChangeHandler = e => {
		setAvailabilityId(e.target.value);
	};

	const imagePathHandler = e => {
		// console.log(fileRef.current.files[0]);
		console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			console.log(encodedFile);
			setImagePath(encodedFile);
		});
	};

	const availabilityOptions = () => {
		console.log(props);
		let availabilityData = props.getAvailabilitiesQuery;
		if (availabilityData.loading) {
			return <option>Loading Availabilities...</option>;
		} else {
			return availabilityData.getAvailabilities.map(availability => {
				return (
					<option
						key={availability.id}
						value={availability.id}
						selected={
							availability.id === availabilityId ? true : false
						}
					>
						{availability.name}
					</option>
				);
			});
		}
	};

	const addVenue = e => {
		e.preventDefault();

		let newVenue = {
			name: name,
			description: description,
			availabilityId: availabilityId
		};
		console.log(newVenue);

		props.createVenueMutation({
			variables: newVenue,
			refetchQueries: [
				{
					query: getVenuesQuery
				}
			]
		});
		setName("");
		setDescription("");
		setAvailabilityId("");

		Swal.fire({
			title: "Venue Updated",
			text: "Venue has been updated",
			type: "success",
			icon: "success",
			position: "top-end",
			html:
				'<a href="/venues"class="button is-success">Go back to Venue</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Book A Venue</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addVenue}>
								<div className="field">
									<label className="label" htmlFor="name">
										Name
									</label>

									<input
										id="name"
										className="input"
										type="text"
										placeholder="Enter venue name"
										onChange={nameChangeHandler}
										value={name}
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="descirption"
									>
										Description
									</label>
									<input
										id="description"
										className="input"
										type="text"
										placeholder="Enter description"
										onChange={descriptionChangeHandler}
										value={description}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="endTime">
										Availability
									</label>
									<div className="select is-fullwidth">
										<select
											onChange={availabilityChangeHandler}
										>
											<option disabled selected>
												Select Availability
											</option>
											{availabilityOptions()}
										</select>
									</div>
								</div>

								<div className="field">
									<label className="label" htmlFor="image">
										Add Image
									</label>
									<div className="control">
										<input
											id="profile"
											className="input"
											type="file"
											accept="image/jpg"
											ref={fileRef}
											onChange={imagePathHandler}
										/>
									</div>
								</div>

								<button
									className="button is-success is-fullwidth"
									type="submit"
								>
									Add Venue
								</button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getAvailabilitiesQuery, { name: "getAvailabilitiesQuery" }),
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(createVenueMutation, { name: "createVenueMutation" })
)(AddVenue);
