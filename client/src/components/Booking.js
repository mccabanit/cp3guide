import React, { useState } from "react";
import {
	Navbar,
	Button,
	Columns,
	Card,
	Container,
	Heading
} from "react-bulma-components";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import "bootstrap";
import { Table } from "react-bootstrap";

// queries
import {
	getBookingsQuery,
	getStatusesQuery,
	getVenuesQuery,
	getUsersQuery
} from "../queries/queries";
import {
	deleteBookingMutation,
	updateBookingMutation
} from "../queries/mutations";

const moment = require("moment");

const Booking = props => {
	console.log(props);
	// const [visible, setVisible] = useState(true);
	const [date, setDate] = useState("");
	const [startTime, setStartTime] = useState("");
	const [endTime, setEndTime] = useState("");
	const [statusId, setStatusId] = useState("");
	const [venueId, setVenueId] = useState("");
	const [userId, setUserId] = useState("");

	const dateChangeHandler = e => {
		setDate(e.target.value);
	};

	const startTimeChangeHandler = e => {
		setStartTime(e.target.value);
	};

	const endTimeChangeHandler = e => {
		setEndTime(e.target.value);
	};

	const statusChangeHandler = e => {
		setStatusId(e.target.value);
	};

	const venueIdChangeHandler = e => {
		setVenueId(e.target.value);
	};

	const userIdChangeHandler = e => {
		setUserId(e.target.value);
	};

	// let date = moment();
	// console.log(date.format('ll'));

	const bookingData = props.getBookingsQuery.getBookings
		? props.getBookingsQuery.getBookings
		: [];
	console.log(bookingData);

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];
	console.log(userData);

	const venueOptions = () => {
		console.log(props);
		let venueData = props.getVenuesQuery ? props.getVenuesQuery : [];
		if (venueData.loading) {
			return <option>Loading Venues...</option>;
		} else {
			return venueData.getVenues.map(venue => {
				return (
					<option key={venue.id} value={venue.id}>
						{venue.name}
					</option>
				);
			});
		}
	};

	const statusOptions = () => {
		// let teamData
		console.log(props);
		let statusData = props.getStatusesQuery ? props.getStatusesQuery : [];
		if (statusData.loading) {
			return <option>Loading Statuses...</option>;
		} else {
			return statusData.getStatuses.map(status => {
				return (
					<option key={status.id} value={status.id}>
						{status.name}
					</option>
				);
			});
		}
	};

	const deleteBookingHandler = e => {
		console.log("deleting a booking...");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			allowEscapeKey: true,
			allowEnterKey: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteBookingMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getBookingsQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"This booking has been deleted.",
					"success"
				);
			}
		});
	};

	if (props.getBookingsQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "Fetching bookings...",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}

	return (
		<div>
			<Heading>
				<h1 className="heading-text">Bookings</h1>
			</Heading>
			<table class="table table-hover table-responsive">
				<thead>
					<tr>
						<th scope="col">Booking No.</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">Venue</th>
						<th scope="col">Date</th>
						<th scope="col">Start Time</th>
						<th scope="col">End Time</th>
						<th scope="col">Status</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					{bookingData.map(booking => {
						// console.log(booking);
						// let user = booking.users[] ? booking.users[] : "loading";
						let user = booking.users[0]
							? booking.users[0]
							: "loading";
						let venue = booking.venues[0]
							? booking.venues[0]
							: "loading";
						let status = booking.statuses[0]
							? booking.statuses[0]
							: "loading";
						let formattedDate = moment(booking.date).format(
							"dddd, MMMM Do YYYY"
						);
						let formattedStartTime = moment(
							booking.startTime
						).format("h:mm A");
						let formattedEndTime = moment(booking.endTime).format(
							"h:mm A"
						);

						return (
							<tr key={booking.id}>
								<td>{booking.id.toUpperCase()}</td>
								<td>{user.firstName}</td>
								<td>{user.lastName}</td>
								<td>{venue.name}</td>
								<td>{formattedDate}</td>
								<td>{formattedStartTime}</td>
								<td>{formattedEndTime}</td>
								<td>{status.name.toUpperCase()}</td>
								<td>
									<Link to={"/booking/update/" + booking.id}>
										<Button color="link" fullwidth>
											Update
										</Button>
									</Link>
									<Button
										onClick={deleteBookingHandler}
										id={booking.id}
										color="danger"
										fullwidth
									>
										Delete
									</Button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default compose(
	graphql(getBookingsQuery, { name: "getBookingsQuery" }),
	graphql(getStatusesQuery, { name: "getStatusesQuery" }),
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(deleteBookingMutation, { name: "deleteBookingMutation" }),
	graphql(updateBookingMutation, { name: "updateBookingMutation" })
)(Booking);
