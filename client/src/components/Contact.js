import React from "react";
import { Container, Columns, Heading, Button } from "react-bulma-components";

const Contact = () => {
	return (
		<Container className="contact has-text-centered">
			<Heading>
				<h1 className="heading-text">Contact Us</h1>
			</Heading>
			<hr />
			<Columns>
				<Columns.Column className="animated fadeInLeft">
					<form>
						<div className="field">
							<label className="label has-text-dark">Name</label>
							<div className="control">
								<input
									id="name"
									className="input"
									type="text"
								/>
							</div>
						</div>
						<div className="field">
							<label className="label has-text-dark">Email</label>
							<div className="control">
								<input
									id="email"
									className="input"
									type="text"
								/>
							</div>
						</div>

						<div className="field">
							<label className="label has-text-dark">
								Message
							</label>
							<div className="control">
								<textarea
									className="textarea"
									placeholder="Enter message here"
									rows="5"
								></textarea>
							</div>
						</div>

						<Button
							type="submit"
							color="success"
							className="is-fullwidth"
						>
							Submit
						</Button>
					</form>
				</Columns.Column>
				<Columns.Column className="animated fadeInRight">
					<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.728434624737!2d121.03293391545695!3d14.614539889793623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b7cd159cb953%3A0xba72bd0c74a1c798!2sSt.%20Paul%20University%20Quezon%20City!5e0!3m2!1sen!2sph!4v1575868296219!5m2!1sen!2sph"
						width="100%"
						height="600"
						style={{ border: 0 }}
					></iframe>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default Contact;
