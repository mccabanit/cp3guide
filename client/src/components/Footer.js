import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";
import { Bootstrap, Grid, Row, Col, Table } from "react-bootstrap";

const Footer = props => {
	// const [visible, setVisible] = useState(true);
	const [open, setOpen] = useState(false);
	console.log(props.username);
	let customLink = props.username ? (
		<Link to="/logout" className="navbar-item">
			{props.username}, logout?
		</Link>
	) : (
		<Link to="/login" className="navbar-item">
			Login
		</Link>
	);
	console.log(customLink);
	return (
		<footer class="page-footer font-small teal pt-4">
			<div class="container-fluid text-center text-md-left">
				<div class="text-center">
					<h5 class="text-uppercase font-weight-bold">Location</h5>
					<p>
						Aurora Boulevard corner Gilmore Avenue, New Manila,
						Quezon City, Philippines
					</p>
				</div>
			</div>

			<div class="text-center py-3">
				<i class="fab fa-facebook-f"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<i class="fab fa-twitter"></i>
				<br />
				<i class="fas fa-phone"></i>&nbsp;+632
				7267986&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<br />
				<i class="fas fa-envelope"></i>&nbsp;marketing@spuqc.edu.ph
			</div>
			<div class="footer-copyright text-center py-3">
				© 2019 Copyright: MCCABANIT
			</div>
		</footer>
	);
};

export default Footer;
