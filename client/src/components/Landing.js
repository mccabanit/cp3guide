import React, { useEffect, useState } from "react";
import { Container, Columns, Card, Table } from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
// import { Jumbotron, Button } from "bootstrap";
import { Jumbotron, Button } from "reactstrap";

// import
// import { logInMutation } from "../queries/mutations";
// queries
// import { getMembersQuery } from "../queries/queries";

const Landing = props => {
	return (
		<Jumbotron>
			<h1>Hello, world!</h1>
			<p>
				This is a simple hero unit, a simple jumbotron-style component
				for calling extra attention to featured content or information.
			</p>
			<p>
				<Button variant="primary">Learn more</Button>
			</p>
		</Jumbotron>
	);
};

export default compose()(Landing);
// graphql(getMembersQuery, { name: "getMembersQuery" }),
// graphql(logInMutation, { name: "logInMutation" })
