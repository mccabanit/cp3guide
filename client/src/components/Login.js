import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import { Redirect } from "react-router-dom";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

// import
import { logInMutation } from "../queries/mutations";
// queries

const Login = props => {
	console.log(props);
	// hooks
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [logInSuccess, setLogInSuccess] = useState(false);

	useEffect(() => {
		console.log("email: " + email);
		console.log("password: " + password);
	});

	const emailChangeHandler = e => {
		setEmail(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();

		props
			.logInMutation({
				variables: {
					email: email,
					password: password
				}
			})
			.then(response => {
				console.log(response);
				let data = response.data.logInUser;

				if (data === null) {
					Swal.fire({
						icon: "error",
						title: "Login Failed",
						text: "wrong credentials"
					});
				} else {
					Swal.fire({
						icon: "success",
						title: "Login Successful",
						text: "redirecting to homepage"
					}).then(function() {
						window.location = "/home";
					});
					localStorage.setItem("email", data.email);
					localStorage.setItem("password", data.password);
					props.updateSession();
					setLogInSuccess(true);
				}
			});
	};

	if (!logInSuccess) {
		console.log("something went wrong");
	} else {
		console.log("successful login");
		return <Redirect to="/home" />;
	}
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Login</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={submitFormHandler}>
								<div className="field">
									<label className="label" htmlFor="teamName">
										Email
									</label>
									<input
										id="email"
										className="input"
										type="text"
										value={email}
										onChange={emailChangeHandler}
										placeholder="Enter email"
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="teamName">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="password"
										value={password}
										onChange={passwordChangeHandler}
										placeholder="Enter password"
									/>
								</div>

								<Button type="submit" color="success" fullwidth>
									Login
								</Button>
							</form>
							<p>
								Don't have an account?{" "}
								<a href="/register">Register here</a>!
							</p>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(logInMutation, { name: "logInMutation" }))(
	Login
);
