import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBar = props => {
	// const [visible, setVisible] = useState(true);
	const [open, setOpen] = useState(false);
	console.log(props.username);
	let customLink = props.username ? (
		<Link to="/logout" className="navbar-item">
			{props.username}, logout?
		</Link>
	) : (
		<Link to="/login" className="navbar-item">
			Login
		</Link>
	);
	console.log(customLink);
	return (
		<Navbar color="black" active={open}>
			<Navbar.Brand>
				<Link to="/" className="navbar-item">
					<strong>Tipol Venue Booking System</strong>
				</Link>

				<Navbar.Burger
					className="has-text-primary"
					active={open}
					onClick={() => {
						setOpen(!open);
					}}
				/>
			</Navbar.Brand>

			<Navbar.Menu>
				<Navbar.Container position="end">
					<Link to="/venues" className="navbar-item">
						Venues
					</Link>
					<Link to="/add_booking" className="navbar-item">
						Book Now
					</Link>
					<Link to="/bookings" className="navbar-item">
						Manage Bookings
					</Link>
					<Link to="/users" className="navbar-item">
						Manage Users
					</Link>

					<Link to="/contact_us" className="navbar-item">
						Contact Us
					</Link>
					{customLink}
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default NavBar;
