import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

// import
import { createUserMutation } from "../queries/mutations";
// queries
import { getUsersQuery } from "../queries/queries";

const Register = props => {
	console.log(props);
	// hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	useEffect(() => {
		console.log("email of the team: " + email);
		console.log("password: " + password);
		console.log("confirm password: " + confirmPassword);
		console.log("first name: " + firstName);
		console.log("last name: " + lastName);
	});

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const confirmPasswordChangeHandler = e => {
		setConfirmPassword(e.target.value);
	};

	const registerUser = e => {
		e.preventDefault();

		console.log("hello");

		let newUser = {
			firstName: firstName,
			lastName: lastName,
			email: email,
			password: password,
			confirmPassword: confirmPassword,
			roleId: "5de06eaaa598721e37bafb7f"
		};

		console.log(newUser);

		props.createUserMutation({
			variables: newUser,
			refetchQueries: [
				{
					query: getUsersQuery
				}
			]
		});

		Swal.fire({
			icon: "success",
			position: "top-end",
			title: "Registered successfully"
		});
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>Register</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={registerUser}>
								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										First Name
									</label>
									<input
										id="firstName"
										className="input"
										type="text"
										placeholder="Enter first name"
										onChange={firstNameChangeHandler}
										value={firstName}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="lastName">
										Last Name
									</label>
									<input
										id="lastName"
										className="input"
										type="text"
										placeholder="Enter last name"
										onChange={lastNameChangeHandler}
										value={lastName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="email">
										Email
									</label>
									<input
										id="email"
										className="input"
										type="text"
										placeholder="Enter email"
										value={email}
										onChange={emailChangeHandler}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="password"
										placeholder="Enter password"
										value={password}
										onChange={passwordChangeHandler}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="password">
										Confirm Password
									</label>
									<input
										id="confirmPassword"
										className="input"
										type="password"
										placeholder="Confirm password"
										value={confirmPassword}
										onChange={confirmPasswordChangeHandler}
									/>
								</div>

								<Button type="submit" color="success" fullwidth>
									Register
								</Button>
							</form>
							<p>
								Already have an account?{" "}
								<a href="/login">Login here</a>!
							</p>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(createUserMutation, { name: "createUserMutation" })
)(Register);
