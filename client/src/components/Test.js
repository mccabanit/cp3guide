import React, { useState, useEffect } from "react";
import {
	Navbar,
	Button,
	Columns,
	Card,
	Container
} from "react-bulma-components";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import { Modal } from "react-bootstrap";

import ReactDOM from "react-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

const Test = props => {
	return (
		<Carousel infiniteLoop autoPlay swipeable={false} className="carousel">
			<div>
				<img src="https://pbs.twimg.com/media/DlRHJqgVsAAzbvy.jpg" />
			</div>
			<div>
				<img src="https://spuqc.edu.ph/wp-content/uploads/IMG_0227-min-e1528853399454.jpg" />
			</div>
			<div>
				<img src="https://lakansining.files.wordpress.com/2019/02/26-1984-sr.-erlinda-bandril-spc-spuqc-gym-multipurpose-hall.jpg?w=1200" />
			</div>
		</Carousel>
	);
};

export default compose()(Test);
