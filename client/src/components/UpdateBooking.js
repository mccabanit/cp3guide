import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
// these 2 imports are important okay???
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

import DatePicker from "react-datepicker";
// import TimePicker from 'react-time-picker';
import "react-datepicker/dist/react-datepicker.css";
// queries
import {
	getBookingQuery,
	getVenuesQuery,
	getStatusesQuery,
	getUsersQuery
} from "../queries/queries";
// mutations
import { updateBookingMutation } from "../queries/mutations";

const UpdateBooking = props => {
	console.log(props);
	const moment = require("moment-timezone");
	const now = moment().tz("Asia/Taipei");
	// hooks
	const [date, setDate] = useState("");
	const [startTime, setStartTime] = useState("");
	const [endTime, setEndTime] = useState("");
	const [venueId, setVenueId] = useState("");
	const [statusId, setStatusId] = useState("");
	const [userId, setUserId] = useState("");

	const dateChangeHandler = e => {
		setDate(e.target.value);
	};

	const startTimeChangeHandler = e => {
		setStartTime(e.target.value);
	};

	const endTimeChangeHandler = e => {
		setEndTime(e.target.value);
	};

	const venueChangeHandler = e => {
		setVenueId(e.target.value);
	};

	const statusChangeHandler = e => {
		setStatusId(e.target.value);
	};

	const userChangeHandler = e => {
		setUserId(e.target.value);
	};

	const bookingData = props.getBookingQuery.getBooking
		? props.getBookingQuery.getBooking
		: {};
	console.log(bookingData);

	if (!props.getBookingQuery.loading) {
		const setDefaultValues = () => {
			setDate(bookingData.date);
			// setStartTime(bookingData.startTime);
			setStatusId(bookingData.setStatusId);
			setVenueId(bookingData.setVenueId);
		};
		if (date === "") {
			setDefaultValues();
		}
	}

	useEffect(() => {
		console.log(date);
		console.log(startTime);
		console.log(endTime);
		console.log(venueId);
	});

	const venueOptions = () => {
		let venueData = props.getVenuesQuery ? props.getVenuesQuery : [];
		if (venueData.loading) {
			return <option>Loading Venues...</option>;
		} else {
			return venueData.getVenues.map(venue => {
				return (
					<option
						key={venue.id}
						value={venue.id}
						selected={venue.id === venueId ? true : false}
					>
						{venue.name}
					</option>
				);
			});
		}
	};

	const statusOptions = () => {
		let statusData = props.getStatusesQuery ? props.getStatusesQuery : [];
		if (statusData.loading) {
			return <option>Loading Statuses...</option>;
		} else {
			return statusData.getStatuses.map(status => {
				return (
					<option
						key={status.id}
						value={status.id}
						selected={status.id === statusId ? true : false}
					>
						{status.name}
					</option>
				);
			});
		}
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatedBooking = {
			// id: member.id,
			id: bookingData.id,
			date: date,
			startTime: startTime,
			endTime: endTime,
			venueId: venueId,
			statusId: statusId,
			// userId: userId
			userId: "5de5f64cbf4c1431402f0112"
		};
		console.log(updatedBooking);

		props.updateBookingMutation({
			variables: updatedBooking,
			refetchQueries: [
				{
					query: getBookingQuery
				}
			]
		});

		setDate("");
		setStartTime("");
		setEndTime("");
		setStatusId("");
		setVenueId("");

		Swal.fire({
			title: "Booking Updated",
			text: "Booking has been updated",
			type: "success",
			icon: "success",
			position: "top-end",
			html: '<a href="/"class="button is-success">Go back to Booking</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Update Booking
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										Date
									</label>

									<DatePicker
										className="control is-fullwidth input"
										showPopperArrow={false}
										// selected={date}
										onChange={date => setDate(date)}
										showDisabledMonthNavigation
										minDate={new Date()}
										value={date}
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="startTime"
									>
										Start Time
									</label>
									<DatePicker
										className="control input"
										selected={startTime}
										onChange={startTime =>
											setStartTime(startTime)
										}
										showTimeSelect
										showTimeSelectOnly
										timeIntervals={30}
										dateFormat="h:mm aa"
										min={startTime}
										value={startTime}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="endTime">
										End Time
									</label>
									<DatePicker
										className="control input"
										selected={endTime}
										onChange={endTime =>
											setEndTime(endTime)
										}
										showTimeSelect
										showTimeSelectOnly
										timeIntervals={30}
										dateFormat="h:mm aa"
										min={startTime}
										max={endTime}
										// value={endTime}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="venue">
										Venue
									</label>
									<div className="select">
										<select onChange={venueChangeHandler}>
											<option disabled selected>
												Select Venue
											</option>
											{venueOptions()}
										</select>
									</div>
								</div>

								<div className="field">
									<div className="control">
										<label
											className="label"
											htmlFor="venue"
										>
											Venue
										</label>
										<div className="select">
											<select
												onChange={statusChangeHandler}
											>
												<option disabled selected>
													Select Status
												</option>
												{statusOptions()}
											</select>
										</div>
									</div>
								</div>

								<button
									className="button is-success is-fullwidth"
									type="submit"
								>
									Update
								</button>
								<Link to="/users">
									<button className="button is-danger is-fullwidth">
										Cancel
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(getStatusesQuery, { name: "getStatusesQuery" }),
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(updateBookingMutation, { name: "updateBookingMutation" }),
	graphql(getBookingQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getBookingQuery"
	})
)(UpdateBooking);
