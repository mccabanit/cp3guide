import React, { useEffect, useState } from "react";
import { Section, Heading, Columns, Container } from "react-bulma-components";
import { Modal, Card, Button } from "react-bootstrap";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import {
	getBookingQuery,
	getBookingsQuery,
	getAvailabilitiesQuery,
	getVenuesQuery
} from "../queries/queries";
// mutations
import { updateBookingMutation } from "../queries/mutations";

// component
import UpdateBooking from "./UpdateBooking";

const UpdateBookingModal = props => {
	console.log(props);

	// for modals!!
	const [show, setShow] = useState(false);
	const [showUpdate, setShowUpdate] = useState(false);
	// close modal
	const handleClose = () => {
		setShow(false);
		window.location.reload(false);
	};
	// open modal
	const handleShow = () => setShow(true);

	// select id
	const [selectedBookingId, setSelectedBookingId] = useState(null);

	const updateHandleClose = () => {
		setShowUpdate(false);
		window.location.reload(false);
	};
	const updateHandleShow = e => {
		setSelectedBookingId(e.target.id);
		console.log(e.target.id);
		setShowUpdate(true);
	};

	const selectBookingId = e => {
		console.log(e.target.id);
		setSelectedBookingId(e.target.id);
	};

	const bookingData = props.getBookingsQuery.getBookings
		? props.getBookingsQuery.getBookings
		: [];
	console.log(bookingData);

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Modal show={showUpdate} onHide={updateHandleClose}>
						<Modal.Header closeButton>
							<Modal.Title>Update Booking</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<UpdateBooking
								selectedBookingId={selectedBookingId}
								close={handleClose}
							/>
						</Modal.Body>
					</Modal>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getAvailabilitiesQuery, { name: "getAvailabilitiesQuery" }),
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(getBookingsQuery, { name: "getBookingsQuery" }),
	graphql(updateBookingMutation, { name: "updateBookingMutation" }),
	graphql(getBookingQuery, {
		options: props => {
			// retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getVenueQuery"
	})
)(UpdateBookingModal);
