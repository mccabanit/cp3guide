import React, { useEffect, useState } from "react";
import {
	Section,
	Heading,
	Card,
	Columns,
	Container
} from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import { getUserQuery, getRolesQuery } from "../queries/queries";
// mutations
import { updateUserMutation } from "../queries/mutations";

//components

const UpdateUser = props => {
	// hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	const userData = props.getUserQuery.getUser
		? props.getUserQuery.getUser
		: {};
	console.log(userData.firstName);

	if (!props.getUserQuery.loading) {
		const setDefaultValues = () => {
			setFirstName(userData.firstName);
			setLastName(userData.lastName);
			setEmail(userData.email);
			setPassword(userData.password);
		};
		if (
			email === "" &&
			firstName === "" &&
			lastName === "" &&
			password === "" &&
			confirmPassword === ""
		) {
			setDefaultValues();
		}
	}

	useEffect(() => {
		console.log(firstName);
		console.log(lastName);
		console.log(email);
		console.log(password);
		console.log(confirmPassword);
	});

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const confirmPasswordChangeHandler = e => {
		setConfirmPassword(e.target.value);
	};

	const formSubmitHandler = e => {
		e.preventDefault();
		console.log(e.target.id);
		let updatedUser = {
			id: userData.id,
			email: email,
			firstName: firstName,
			lastName: lastName,
			password: password,
			confirmPassword: confirmPassword
		};

		if (password != confirmPassword) {
			console.log("passwords should be same");
		}
		console.log("this is a new member");
		console.log(updatedUser);
		props.updateUserMutation({
			variables: updatedUser,
			refetchQueries: [
				{
					query: getUserQuery
				}
			]
		});

		setFirstName("");
		setLastName("");
		setEmail("");
		setPassword("");
		setConfirmPassword("");

		Swal.fire({
			icon: "success",
			title: "You have successfully updated your information",
			timer: 1500,
			showConfirmButton: false
		});
		//.then(()=>{
		//    window.location.href = "/users";
		// })

		console.log("updated user");
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Update User Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<div className="field">
									<label
										className="label"
										htmlFor="firstName"
									>
										First Name
									</label>
									<input
										className="input"
										type="text"
										onChange={firstNameChangeHandler}
										value={firstName}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="lastName">
										Last Name
									</label>
									<input
										className="input"
										type="text"
										onChange={lastNameChangeHandler}
										value={lastName}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="email">
										Email
									</label>
									<input
										className="input"
										type="text"
										value={email}
										onChange={emailChangeHandler}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										className="input"
										type="password"
										value={password}
										onChange={passwordChangeHandler}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="password">
										Confirm Password
									</label>
									<input
										className="input"
										type="password"
										value={confirmPassword}
										onChange={confirmPasswordChangeHandler}
									/>
								</div>
								<button
									className="button is-success is-fullwidth"
									type="submit"
								>
									Update
								</button>
								<Link to="/users">
									<button className="button is-danger is-fullwidth">
										Cancel
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	// graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(updateUserMutation, { name: "updateUserMutation" }),
	graphql(getUserQuery, {
		options: props => {
			// retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getUserQuery"
	})
)(UpdateUser);
