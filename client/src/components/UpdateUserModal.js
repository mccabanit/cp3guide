import React, { useEffect, useState } from "react";
import { Section, Heading, Columns, Container } from "react-bulma-components";
import { Modal, Card, Button } from "react-bootstrap";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import { getUserQuery, getUsersQuery, getRolesQuery } from "../queries/queries";
// mutations
import { updateUserMutation } from "../queries/mutations";

//components
// import CreateUser from "./CreateUser";
// import UpdateUser from "./UpdateUser";
import UpdateUser from "./UpdateUser";

const UpdateUserModal = props => {
	console.log(props);

	// for modals!!
	const [show, setShow] = useState(false);
	const [showUpdate, setShowUpdate] = useState(false);
	// close modal
	const handleClose = () => {
		setShow(false);
		window.location.reload(false);
	};
	// open modal
	const handleShow = () => setShow(true);

	// select id
	const [selectedUserId, setSelectedUserId] = useState(null);

	const updateHandleClose = () => {
		setShowUpdate(false);
		window.location.reload(false);
	};
	const updateHandleShow = e => {
		setSelectedUserId(e.target.id);
		console.log(e.target.id);
		setShowUpdate(true);
	};

	const selectUserId = e => {
		console.log(e.target.id);
		setSelectedUserId(e.target.id);
	};

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];
	console.log(userData);

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Modal show={showUpdate} onHide={updateHandleClose}>
						<Modal.Header closeButton>
							<Modal.Title>Update User</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<UpdateUser
								selectedUserId={selectedUserId}
								close={handleClose}
							/>
						</Modal.Body>
					</Modal>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	graphql(updateUserMutation, { name: "updateUserMutation" }),
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(getUserQuery, {
		options: props => {
			// retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getUserQuery"
	})
)(UpdateUserModal);
