import React, { useEffect, useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Table
} from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import { getVenueQuery, getAvailabilitiesQuery } from "../queries/queries";
// mutations
import { updateVenueMutation } from "../queries/mutations";

const UpdateVenue = props => {
	console.log(props);
	// hooks
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [availabilityId, setAvailabilityId] = useState("");

	const nameChangeHandler = e => {
		setName(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const availabilityChangeHandler = e => {
		setAvailabilityId(e.target.value);
	};

	const venueData = props.getVenueQuery.getVenue
		? props.getVenueQuery.getVenue
		: {};
	console.log(venueData);

	if (!props.getVenueQuery.loading) {
		const setDefaultValues = () => {
			setName(venueData.name);
			setDescription(venueData.description);
			setAvailabilityId(venueData.availabilityId);
		};
		if (name === "") {
			setDefaultValues();
		}
	}

	useEffect(() => {
		console.log(name);
		console.log(description);
		console.log(availabilityId);
	});

	const availabilityOptions = () => {
		let availabilityData = props.getAvailabilitiesQuery;
		if (availabilityData.loading) {
			return <option>Loading Availabilities...</option>;
		} else {
			return availabilityData.getAvailabilities.map(availability => {
				return (
					<option
						key={availability.id}
						value={availability.id}
						selected={
							availability.id === availabilityId ? true : false
						}
					>
						{availability.name}
					</option>
				);
			});
		}
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatedVenue = {
			// id: member.id,
			id: venueData.id,
			name: name,
			description: description,
			availabilityId: availabilityId
		};
		console.log(updatedVenue);

		props.updateVenueMutation({
			variables: updatedVenue,
			refetchQueries: [
				{
					query: getVenueQuery
				}
			]
		});

		setName("");
		setDescription("");
		setAvailabilityId("");

		Swal.fire({
			title: "Venue Updated",
			text: "Venue has been updated",
			type: "success",
			icon: "success",
			position: "top-end",
			html:
				'<a href="/venues"class="button is-success">Go back to Venue</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Update Booking
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<div className="field">
									<label className="label" htmlFor="name">
										Name
									</label>

									<input
										id="name"
										className="input"
										type="text"
										placeholder="Enter venue name"
										onChange={nameChangeHandler}
										value={name}
									/>
								</div>

								<div className="field">
									<label
										className="label"
										htmlFor="descirption"
									>
										Description
									</label>
									<input
										id="description"
										className="input"
										type="text"
										placeholder="Enter description"
										onChange={descriptionChangeHandler}
										value={description}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="endTime">
										End Time
									</label>
									<div className="select is-fullwidth">
										<select
											onChange={availabilityChangeHandler}
										>
											<option disabled selected>
												Select Availability
											</option>
											{availabilityOptions()}
										</select>
									</div>
								</div>

								<button
									className="button is-success is-fullwidth"
									type="submit"
								>
									Update
								</button>
								<Link to="/users">
									<button className="button is-danger is-fullwidth">
										Cancel
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getAvailabilitiesQuery, { name: "getAvailabilitiesQuery" }),
	graphql(updateVenueMutation, { name: "updateVenueMutation" }),
	graphql(getVenueQuery, {
		options: props => {
			// retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getVenueQuery"
	})
)(UpdateVenue);
