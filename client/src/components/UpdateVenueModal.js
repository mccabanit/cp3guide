import React, { useEffect, useState } from "react";
import { Section, Heading, Columns, Container } from "react-bulma-components";
import { Modal, Card, Button } from "react-bootstrap";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
// queries
import {
	getVenueQuery,
	getVenuesQuery,
	getAvailabilitiesQuery
} from "../queries/queries";
// mutations
import { updateVenueMutation } from "../queries/mutations";

// component
import UpdateVenue from "./UpdateVenue";

const UpdateVenueModal = props => {
	console.log(props);

	// for modals!!
	const [show, setShow] = useState(false);
	const [showUpdate, setShowUpdate] = useState(false);
	// close modal
	const handleClose = () => {
		setShow(false);
		window.location.reload(false);
	};
	// open modal
	const handleShow = () => setShow(true);

	// select id
	const [selectedVenueId, setSelectedVenueId] = useState(null);

	const updateHandleClose = () => {
		setShowUpdate(false);
		window.location.reload(false);
	};
	const updateHandleShow = e => {
		setSelectedVenueId(e.target.id);
		console.log(e.target.id);
		setShowUpdate(true);
	};

	const selectVenueId = e => {
		console.log(e.target.id);
		setSelectedVenueId(e.target.id);
	};

	const venueData = props.getVenuesQuery.getVenues
		? props.getVenuesQuery.getVenues
		: [];
	console.log(venueData);

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Modal show={showUpdate} onHide={updateHandleClose}>
						<Modal.Header closeButton>
							<Modal.Title>Update Venue</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<UpdateVenue
								selectedVenueId={selectedVenueId}
								close={handleClose}
							/>
						</Modal.Body>
					</Modal>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getAvailabilitiesQuery, { name: "getAvailabilitiesQuery" }),
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(updateVenueMutation, { name: "updateVenueMutation" }),
	graphql(getVenueQuery, {
		options: props => {
			// retrieve the wildcard id param
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getVenueQuery"
	})
)(UpdateVenueModal);
