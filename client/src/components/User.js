import React, { useState, useEffect } from "react";
import {
	Navbar,
	Button,
	Columns,
	Card,
	Container,
	Heading
} from "react-bulma-components";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import "bootstrap";
import UpdateUser from "./UpdateUser";
import { Modal } from "react-bootstrap";

// queries
import {
	getUsersQuery,
	getRolesQuery,
	getUserQuery,
	getRoleQuery
} from "../queries/queries";
import { deleteUserMutation, updateUserMutation } from "../queries/mutations";

const User = props => {
	// const [visible, setVisible] = useState(true);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [roleId, setRoleId] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	useEffect(() => {
		console.log(email);
		console.log(password);
		console.log(confirmPassword);
		console.log(firstName);
		console.log(lastName);
	});

	// MODALS
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const userData = props.getUsersQuery.getUsers
		? props.getUsersQuery.getUsers
		: [];
	console.log(userData);

	const updateUserHandler = e => {
		let user = props.getUserQuery.getUser ? props.getUserQuery.getUser : {};
		console.log(user);

		if (!props.getUserQuery.loading) {
			const setDefaultValues = () => {
				setFirstName(user.firstName);
				setLastName(user.lastName);
				setEmail(user.email);
				setPassword(user.password);
			};
			if (roleId === "") {
				setDefaultValues();
				console.log("roleId value: " + roleId);
			}

			if (firstName === "") {
				setFirstName(user.firstName);
			}

			if (lastName === "") {
				setFirstName(user.lastName);
			}

			if (email === "") {
				setFirstName(user.email);
			}

			if (password === "") {
				setFirstName(user.password);
			}
		}
	};

	const formSubmitHandler = e => {
		e.preventDefault();

		let updatedUser = {
			// id: user.id,
			id: props.match.params.id,
			firstName: firstName,
			lastName: lastName,
			email: email,
			password: password,
			confirmPassword: confirmPassword
		};
		console.log(updatedUser);

		props.updateUserMutation({
			variables: updatedUser
		});

		Swal.fire({
			title: "User Updated",
			text: "User has been updated",
			type: "success",
			icon: "success",
			position: "top-end",
			html: '<a href="/"class="button is-success">Go back to users</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};

	const deleteUserHandler = e => {
		console.log("deleting a user...");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			allowEscapeKey: true,
			allowEnterKey: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteUserMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUsersQuery
						}
					]
				});
				Swal.fire("Deleted!", "The user has been deleted.", "success");
			}
		});
	};

	if (props.getUsersQuery.loading) {
		let timerInterval;
		Swal.fire({
			title: "Fetching users...",
			timer: 1000,
			timerProgressBar: true,
			onBeforeOpen: () => {
				Swal.showLoading();
			},
			onClose: () => {
				clearInterval(timerInterval);
			}
		}).then(result => {
			if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.timer
			) {
				console.log("I was closed by the timer"); // eslint-disable-line
			}
		});
	}
	return (
		<div>
			<Heading>
				<h1 className="heading-text">Users</h1>
			</Heading>
			<table class="table table-hover table-responsive has-text-centered">
				<thead>
					<tr>
						<th scope="col">User No.</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">Email</th>
						<th scope="col">Role</th>
						<th scope="col">Actions</th>
					</tr>
				</thead>
				<tbody>
					{userData.map(user => {
						let role = user.roles[0] ? user.roles[0] : "loading";
						console.log(role);
						return (
							<tr key={user.id}>
								<td>{user.id.toUpperCase()}</td>
								<td>{user.firstName}</td>
								<td>{user.lastName}</td>
								<td>{user.email}</td>
								<td>{role ? role.name : "unassigned"}</td>
								<td>
									<Link to={"/user/update/" + user.id}>
										<Button
											color="link"
											data-toggle="modal"
											data-target="#exampleModal"
											fullwidth
											onClick={updateUserHandler}
										>
											Update
										</Button>
									</Link>

									<Button
										onClick={deleteUserHandler}
										id={user.id}
										color="danger"
										fullwidth
									>
										Delete
									</Button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

export default compose(
	graphql(getUsersQuery, { name: "getUsersQuery" }),
	graphql(getUserQuery, {
		options: props => {
			console.log(props);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getUserQuery"
	}),
	graphql(getRolesQuery, { name: "getRolesQuery" }),
	graphql(getRoleQuery, { name: "getRoleQuery" }),
	graphql(deleteUserMutation, { name: "deleteUserMutation" }),
	graphql(updateUserMutation, { name: "updateUserMutation" })
)(User);
