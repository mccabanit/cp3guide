import React, { useState, useEffect } from "react";
import {
	Navbar,
	Button,
	Columns,
	Card,
	Container,
	Heading
} from "react-bulma-components";
import { Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import "bootstrap";
import { UpdateVenueModal } from "./UpdateVenueModal";

import { getVenuesQuery, getAvailabilitiesQuery } from "../queries/queries";
import {
	createVenueMutation,
	updateVenueMutation,
	deleteVenueMutation
} from "../queries/mutations";

const Venue = props => {
	const venueData = props.getVenuesQuery.getVenues
		? props.getVenuesQuery.getVenues
		: [];
	console.log(venueData);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [availabilityId, setAvailabilityId] = useState("");
	const fileRef = React.createRef();

	useEffect(() => {});

	const nameChangeHandler = e => {
		setName(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const availabilityChangeHandler = e => {
		setAvailabilityId(e.target.value);
	};

	const availabilityOptions = () => {
		console.log(props);
		let availabilityData = props.getAvailabilitiesQuery
			? props.getAvailabilitiesQuery
			: [];
		if (availabilityData.loading) {
			return <option>Loading Availabilities...</option>;
		} else {
			return availabilityData.getAvailabilities.map(availability => {
				return (
					<option key={availability.id} value={availabilityId}>
						{availability.name}
					</option>
				);
			});
		}
	};

	// const available =
	// 	availability.name === "available" ? (
	// 		<small class="text-muted" color="success">
	// 			{availability.name}
	// 		</small>
	// 	) : (
	// 		<small class="text-muted" color="danger">
	// 			{availability.name}
	// 		</small>
	// 	);

	const addVenue = e => {
		e.preventDefault();

		let newVenue = {
			name: name,
			description: description,
			availabilityId: availabilityId
		};
		console.log(newVenue);

		props.updateVenueMutation({
			variables: newVenue
		});

		Swal.fire({
			title: "Venue Added",
			text: "Venue has been updated",
			type: "success",
			icon: "success",
			position: "top-end",
			html: '<a href="/"class="button is-success">Go back to Venue</a>',
			showCancelButton: false,
			showConfirmButton: false
		});
	};

	const deleteVenueHandler = e => {
		console.log("deleting a venue...");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete this venue?",
			icon: "warning",
			showCancelButton: true,
			allowEscapeKey: true,
			allowEnterKey: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteVenueMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getVenuesQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"This venue has been deleted.",
					"success"
				);
			}
		});
	};

	return (
		<div>
			<Heading>
				<h1 className="heading-text">Venues</h1>
			</Heading>

			{venueData.map(venue => {
				let availability = venue.availabilities[0]
					? venue.availabilities[0]
					: "loading";
				console.log(availability);
				return (
					<div class="card mb-3 has-text-centered">
						<img
							class="card-img-top"
							src="..."
							alt="Card image cap"
						/>
						<div class="card-body">
							<div>
								<h5 class="card-title">{venue.name}</h5>
								<p class="card-text">{venue.description}</p>
								<p class="card-text">
									<small class="text-muted is-success">
										{availability.name}
									</small>
								</p>
							</div>

							<Link to={"/venue/update/" + venue.id}>
								<Button color="link">Update</Button>
							</Link>
							<Button
								onClick={deleteVenueHandler}
								id={venue.id}
								color="danger"
							>
								Delete
							</Button>
						</div>
					</div>
				);
			})}
			<div className="has-text-centered">
				<Link to="/add_Venue">
					<Button type="submit" color="success">
						<i class="fas fa-plus-circle"></i>&nbsp;Add Venue
					</Button>
				</Link>
			</div>
		</div>
	);
};

export default compose(
	graphql(getAvailabilitiesQuery, { name: "getAvailabilitiesQuery" }),
	graphql(getVenuesQuery, { name: "getVenuesQuery" }),
	graphql(deleteVenueMutation, { name: "deleteVenueMutation" }),
	graphql(createVenueMutation, { name: "createVenueMutation" }),
	graphql(updateVenueMutation, { name: "updateVenueMutation" })
)(Venue);
