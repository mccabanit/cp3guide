import { gql } from "apollo-boost";

const createUserMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String!
		$email: String!
		$password: String!
		$confirmPassword: String!
		$roleId: String!
	) {
		createUser(
			firstName: $firstName
			lastName: $lastName
			email: $email
			password: $password
			confirmPassword: $confirmPassword
			roleId: $roleId
		) {
			id
			firstName
			lastName
			email
			password
			confirmPassword
			roleId
		}
	}
`;

const createBookingMutation = gql`
	mutation(
		$venueId: String!
		$statusId: String!
		$userId: String
		$date: Date!
		$startTime: Date!
		$endTime: Date!
	) {
		createBooking(
			venueId: $venueId
			statusId: $statusId
			userId: $userId
			date: $date
			startTime: $startTime
			endTime: $endTime
		) {
			id
			date
			startTime
			endTime
			venueId
			statusId
			userId
		}
	}
`;

const createVenueMutation = gql`
	mutation($name: String!, $description: String!, $availabilityId: String!) {
		createVenue(
			name: $name
			description: $description
			availabilityId: $availabilityId
		) {
			id
			name
			description
			availabilityId
		}
	}
`;

const createRoleMutation = gql`
	mutation($name: String!) {
		createRole(name: $name) {
			id
			name
		}
	}
`;

const createStatusMutation = gql`
	mutation($name: String!) {
		createStatus(name: $name) {
			id
			name
		}
	}
`;

const createAvailabilityMutation = gql`
	mutation($name: String!) {
		createAvailability(name: $name) {
			id
			name
		}
	}
`;

// delete
const deleteUserMutation = gql`
	mutation($id: String!) {
		deleteUser(id: $id)
	}
`;

const deleteBookingMutation = gql`
	mutation($id: String!) {
		deleteBooking(id: $id)
	}
`;

const deleteVenueMutation = gql`
	mutation($id: String!) {
		deleteVenue(id: $id)
	}
`;

const deleteRoleMutation = gql`
	mutation($id: String!) {
		deleteRole(id: $id)
	}
`;

const deleteStatusMutation = gql`
	mutation($id: String!) {
		deleteStatus(id: $id)
	}
`;

const deleteAvailabilityMutation = gql`
	mutation($id: String!) {
		deleteAvailability(id: $id)
	}
`;

// update
const updateUserMutation = gql`
	mutation(
		$id: ID!
		$firstName: String!
		$lastName: String!
		$email: String!
		$password: String!
		$confirmPassword: String!
	) {
		updateUser(
			id: $id
			firstName: $firstName
			lastName: $lastName
			email: $email
			password: $password
			confirmPassword: $confirmPassword
		) {
			firstName
			lastName
			email
			password
			confirmPassword
		}
	}
`;

const updateBookingMutation = gql`
	mutation(
		$id: ID!
		$venueId: String!
		$statusId: String!
		$date: Date!
		$startTime: Date!
		$endTime: Date!
		$userId: userId
	) {
		updateBooking(
			id: $id
			venueId: $venueId
			statusId: $statusId
			date: $date
			startTime: $startTime
			endTime: $endTime
			userId: $userId
		) {
			venueId
			statusId
			date
			startTime
			endTime
			userId
		}
	}
`;

const updateVenueMutation = gql`
	mutation(
		$id: ID!
		$name: String!
		$description: String!
		$availabilityId: String!
	) {
		updateVenue(
			id: $id
			name: $name
			description: $description
			availabilityId: $availabilityId
		) {
			name
			description
			availabilityId
		}
	}
`;

const updateAvailabilityMutation = gql`
	mutation($name: String!) {
		updateAvailability(name: $name) {
			name
		}
	}
`;

const updateRoleMutation = gql`
	mutation($name: String!) {
		updateRole(name: $name) {
			name
		}
	}
`;

const updateStatusMutation = gql`
	mutation($name: String!) {
		updateStatus(name: $name) {
			name
		}
	}
`;

const logInMutation = gql`
	mutation($email: String!, $password: String!) {
		logInUser(email: $email, password: $password) {
			firstName
			lastName
			email
			password
			roleId
		}
	}
`;

export {
	createUserMutation,
	createBookingMutation,
	createVenueMutation,
	createRoleMutation,
	createStatusMutation,
	createAvailabilityMutation,
	deleteUserMutation,
	deleteBookingMutation,
	deleteVenueMutation,
	deleteRoleMutation,
	deleteStatusMutation,
	deleteAvailabilityMutation,
	updateUserMutation,
	updateBookingMutation,
	updateVenueMutation,
	updateRoleMutation,
	updateStatusMutation,
	updateAvailabilityMutation,
	logInMutation
};
