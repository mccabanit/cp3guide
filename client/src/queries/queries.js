import { gql } from "apollo-boost";

const getUsersQuery = gql`
	{
		getUsers {
			id
			firstName
			lastName
			roleId
			email
			password
			confirmPassword
			roles {
				name
			}
		}
	}
`;

const getAvailabilitiesQuery = gql`
	{
		getAvailabilities {
			id
			name
		}
	}
`;

const getBookingsQuery = gql`
	{
		getBookings {
			id
			venueId
			statusId
			userId
			date
			startTime
			endTime
			venues {
				name
			}
			statuses {
				name
			}
			users {
				firstName
				lastName
			}
		}
	}
`;

const getStatusesQuery = gql`
	{
		getStatuses {
			id
			name
		}
	}
`;

const getRolesQuery = gql`
	{
		getRoles {
			id
			name
		}
	}
`;

const getVenuesQuery = gql`
	{
		getVenues {
			id
			name
			description
			availabilityId
			availabilities {
				name
			}
		}
	}
`;

const getUserQuery = gql`
	query($id: ID!) {
		getUser(id: $id) {
			id
			firstName
			lastName
			roleId
			email
			password
			confirmPassword
		}
	}
`;

const getBookingQuery = gql`
	query($id: ID!) {
		getBooking(id: $id) {
			id
			venueId
			statusId
			date
			startTime
			endTime
		}
	}
`;

const getAvailabilityQuery = gql`
	query($id: ID!) {
		getAvailability(id: $id) {
			id
			name
		}
	}
`;

const getRoleQuery = gql`
	query($id: ID!) {
		getRole(id: $id) {
			id
			name
		}
	}
`;

const getStatusQuery = gql`
	query($id: ID!) {
		getStatus(id: $id) {
			id
			name
		}
	}
`;

const getVenueQuery = gql`
	query($id: ID!) {
		getVenue(id: $id) {
			id
			name
			description
			availabilityId
		}
	}
`;

export {
	getBookingsQuery,
	getAvailabilitiesQuery,
	getRolesQuery,
	getStatusesQuery,
	getUsersQuery,
	getVenuesQuery,
	getUserQuery,
	getRoleQuery,
	getStatusQuery,
	getAvailabilityQuery,
	getBookingQuery,
	getVenueQuery
};
