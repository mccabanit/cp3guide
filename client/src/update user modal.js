import React, { useState, useEffect } from "react";
import {graphql} from "react-apollo"
import {Link} from "react-router-dom"
import { Container, Columns  } from "react-bulma-components";
import { flowRight as compose } from "lodash";
import Swal from "sweetalert2";
import { Modal, Card, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
//components
import CreateUser from "./CreateUser";
import UpdateUser from "./UpdateUser";
import {getUsersQuery, getUserQuery} from "./../queries/queries";
import {deleteUserMutation} from "./../queries/mutations";


const ViewUsers = props=> {
	//modal 
	console.log(props.getUserQuery)

	const [show, setShow] = useState(false)
	const [showUpdate, setShowUpdate] = useState(false)
	const handleClose = () => {
		setShow(false)
		window.location.reload(false)
		};
  	const handleShow = () => setShow(true);

  	const [selectedUserId, setSelectedUserId] = useState(null);
	const updateHandleClose = () => {
		setShowUpdate(false)
		window.location.reload(false)
	};
  	const updateHandleShow = e => {  		
	  	setSelectedUserId(e.target.id)
		   	console.log(e.target.id)
  		setShowUpdate(true)
  	}
  	console.log(props)
  

  const selectUserId = e =>{
  	console.log(e.target.id)
  	setSelectedUserId(e.target.id)
  }
  


	const userData = props.getUsersQuery.getUsers ? props.getUsersQuery.getUsers:[]
	// console.log(userData)

	const deleteMemberHandler = e =>{
			console.log(e.target.id)
			let id = e.target.id;
		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteUserMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUsersQuery
						}
					]
				})
				Swal.fire("Deleted!", "Member has been deleted.", "success");
			}
		})
		
	}
	return(
		<Container>
			<Columns.Column size={2} offset={10}>				
				<Button color="info" onClick={handleShow}>
					Create User
				</Button>

				<Modal show={show} onHide={handleClose}>
			        <Modal.Header closeButton>
			          <Modal.Title>Register New User</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        	<CreateUser close={handleClose} />
			        </Modal.Body>	    
		     	</Modal>

		     <Modal show={showUpdate} onHide={updateHandleClose}>
		        <Modal.Header closeButton>
		          <Modal.Title>Update User Details</Modal.Title>
		        </Modal.Header>
		        <Modal.Body>
		        	<UpdateUser selectedUserId={selectedUserId} close={handleClose}  />
		        </Modal.Body>
		     </Modal>				
			</Columns.Column>
				<h2> Users </h2>
			<div className="row">

				{userData.map(user => {
					return(	
						<div className="col-md-4">			
							<Card key={user.id} className="my-3">
							  <Card.Header as="h2" className=""> {user.username ? user.username:"no data"}</Card.Header>
							  <Card.Body>											    
							    <Card.Text>
							     Email: {user.email ? user.email:"no data"}
							    </Card.Text>
							    <Card.Text>
							    First Name: {user.firstName? user.firstName:"no data"}
							    </Card.Text>
							    <Card.Text>
							    Last Name:{user.lastName ? user.lastName:"no data"}
							    </Card.Text>
							    <Card.Text>
							   Contact Number: {user.contactNumber ? user.contactNumber:"no data"} 
							    </Card.Text>
							    <div className="row">
							    	<Button className="btn btn-info" onClick={updateHandleShow} id={user.id}>
											Update
										</Button>
										<Button
											className="btn btn-danger"
											onClick={deleteMemberHandler}
											id={user.id}
										>
											Delete
										</Button>
								  </div>
							  </Card.Body>
							</Card>
						</div>
					)
				})}
			</div>
		</Container>
	)
}

export default compose(
	graphql(getUsersQuery, {name: "getUsersQuery"}),
	graphql(deleteUserMutation, {name: "deleteUserMutation"}),
	graphql(getUserQuery, {name: "getUserQuery"})
	)(ViewUsers)