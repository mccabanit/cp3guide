// declare dependencies
const express = require('express')

// initialize express app
const app = express()

// use the mongoose library
const mongoose = require('mongoose')

const port = 3000;
// database connection
// local
// mongoose.connect("mongodb://localhost:27017/merng_tracker", {useNewUrlParser:true})
// online
mongoose.connect("mongodb+srv://dbUserAdmin:dbUserAdminPassword@nosqlsession-naav9.mongodb.net/bookingsystem?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true
	})


// YOU CAN'T CONNECT LOCAL AND ONLINE AT THE SAME TIME

// check if connection is successful
mongoose.connection.once("open", () => {
	console.log("Connection to the MongoDB server is successful.")
})

// import instantiation of the apollo server
const server = require("./queries/queries")

// the app will be served by the apollo server instead of express
server.applyMiddleware({
	app,
	// path: "/batch43" // if you want to change the project name, you can edit here
})

// server initialization
app.listen(4000, () => {
	// console.log("serving your graphQL project")
	 console.log(`🚀  Server ready at http://localhost:4000${server.graphqlPath}`);
})