const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const soft_delete = require('mongoose-softdelete')

const bookingSchema = new Schema(
	{
		// date : {type: Date, required: true},
		date: { type: Date, required: true, timezone: "Asia/Taipei" },
		startTime: { type: Date, required: true },
		endTime: { type: Date, required: true },
		statusId: { type: String, required: true },
		venueId: { type: String, required: true },
		userId: { type: String, required: true }
	},
	{
		timestamps: true
	}
);

// export model as a module(model name, ModelSchema)
module.exports = mongoose.model("Booking", bookingSchema);
