const mongoose = require("mongoose")
const Schema = mongoose.Schema 
// const soft_delete = require('mongoose-softdelete')


const statusSchema = new Schema({
	name : {type: String, required: true}},
	{
	timestamps: true,
	collection: "statuses"
	})
// export model as a module(model name, ModelSchema)
module.exports = mongoose.model("Status", statusSchema); 
