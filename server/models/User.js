const mongoose = require("mongoose")
const Schema = mongoose.Schema 
// const soft_delete = require('mongoose-softdelete')


const userSchema = new Schema({
	firstName : {type: String, required: true},
	lastName : {type: String, required: true},
	email : {type: String, required: true, unique: true},
	password : {type: String, required: true},
	confirmPassword : {type: String, required: true},
	roleId : {type: String, required: true}},
	{
	timestamps: true
	})

// export model as a module(model name, ModelSchema)
module.exports = mongoose.model("User", userSchema); 
