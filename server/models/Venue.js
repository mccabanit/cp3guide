const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// const soft_delete = require('mongoose-softdelete')

const venueSchema = new Schema(
	{
		name: { type: String, required: true },
		description: { type: String, required: true },
		availabilityId: { type: String, required: true },
		imageLocation: { type: String }
	},
	{
		timestamps: true
	}
);

// export model as a module(model name, ModelSchema)
module.exports = mongoose.model("Venue", venueSchema);
