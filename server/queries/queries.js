const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcrypt");

// mongoose models
const Booking = require("../models/Booking");
const Role = require("../models/Role");
const Status = require("../models/Status");
const User = require("../models/User");
const Venue = require("../models/Venue");
const Availability = require("../models/Availability");

const customScalarResolver = {
	Date: GraphQLDateTime
};

// CRUD
// type Query == Retrieve/Read
// type Mutation == Create / Update /Delete

const typeDefs = gql`
	# this is a comment
	# the type Query is the root of all GraphQL queries
	# this is used for executing "GET" requests

	scalar Date

	type BookingType {
		id: ID
		date: Date!
		startTime: Date!
		endTime: Date!
		statusId: String!
		venueId: String!
		userId: String
		statuses: [StatusType]
		venues: [VenueType]
		users: [UserType]
	}

	type RoleType {
		id: ID
		name: String!
	}

	type StatusType {
		id: ID
		name: String!
	}

	type AvailabilityType {
		id: ID
		name: String!
	}

	type UserType {
		id: ID
		firstName: String!
		lastName: String!
		email: String!
		password: String!
		confirmPassword: String!
		roleId: String!
		roles: [RoleType]
	}

	type VenueType {
		id: ID
		name: String!
		description: String!
		availabilityId: String!
		availabilities: [AvailabilityType]
	}

	type Query {
		getBookings: [BookingType]
		getRoles: [RoleType]
		getStatuses: [StatusType]
		getAvailabilities: [AvailabilityType]
		getUsers: [UserType]
		getVenues: [VenueType]

		#retrieve single document
		getBooking(id: ID!): BookingType
		getRole(id: ID!): RoleType
		getStatus(id: ID!): StatusType
		getUser(id: ID!): UserType
		getVenue(id: ID!): VenueType
		getAvailability(id: ID!): AvailabilityType
	}

	# CUD functionality
	# we are mutating the server/database

	type Mutation {
		createRole(name: String!): RoleType
		createStatus(name: String!): StatusType
		createAvailability(name: String!): AvailabilityType
		createBooking(
			date: Date!
			startTime: Date!
			endTime: Date!
			venueId: String!
			statusId: String!
			userId: String
		): BookingType
		createUser(
			firstName: String!
			lastName: String!
			email: String!
			password: String!
			confirmPassword: String!
			roleId: String!
		): UserType
		createVenue(
			name: String!
			description: String!
			availabilityId: String!
		): VenueType
		updateBooking(
			id: ID
			date: Date!
			startTime: Date!
			endTime: Date!
			venueId: String!
			statusId: String!
			statusId: String!
		): BookingType
		updateVenue(
			id: ID
			name: String!
			description: String!
			availabilityId: String!
		): VenueType
		updateUser(
			id: ID
			firstName: String!
			lastName: String!
			email: String!
			password: String!
			confirmPassword: String!
		): UserType
		deleteUser(id: String): Boolean
		deleteBooking(id: String): Boolean
		deleteVenue(id: String): Boolean

		logInUser(email: String!, password: String!): UserType
	}
`;

const resolvers = {
	// what are we going to do when the query is executed
	Query: {
		getBookings: () => {
			return Booking.find({});
		},
		getRoles: () => {
			return Role.find({});
		},
		getStatuses: () => {
			return Status.find({});
		},
		getAvailabilities: () => {
			return Availability.find({});
		},
		getUsers: () => {
			return User.find({});
		},
		getVenues: () => {
			return Venue.find({});
		},

		getBooking: (_, args) => {
			console.log("you just executed getBOoking");
			console.log(args.id);

			return Booking.findById(args.id);
		},

		getRole: (parent, args) => {
			console.log(args);
			console.log("hoy nag role query ka");

			// return Team.findById(args.id)
			return Role.findOne({ _id: args.id });
		},

		getStatus: (parent, args) => {
			console.log(args);
			console.log("this is status query");

			return Status.findById(args.id);
		},

		getAvailability: (parent, args) => {
			console.log(args);
			console.log("this is availability query");

			return Availability.findById(args.id);
		},

		getUser: (parent, args) => {
			console.log(args);
			console.log("this is user query");

			return User.findById(args.id);
		},

		getVenue: (parent, args) => {
			console.log(args);
			console.log("venue query");

			return Venue.findById(args.id);
		}
	},

	Mutation: {
		createBooking: (_, args) => {
			// console.log(args)

			let newBooking = new Booking({
				date: args.date,
				startTime: args.startTime,
				endTime: args.endTime,
				statusId: args.statusId,
				venueId: args.venueId,
				userId: args.userId
			});

			console.log(newBooking);

			return newBooking.save();
		},
		createRole: (_, args) => {
			// console.log(args)

			let newRole = new Role({
				name: args.name
			});

			console.log(newRole);

			return newRole.save();
		},
		createStatus: (_, args) => {
			// console.log(args)

			let newStatus = new Status({
				name: args.name
			});

			console.log(newStatus);

			return newStatus.save();
		},

		createAvailability: (_, args) => {
			// console.log(args)

			let newAvailability = new Availability({
				name: args.name
			});

			console.log(newAvailability);

			return newAvailability.save();
		},

		createUser: (_, args) => {
			console.log(args);

			let newUser = new User({
				firstName: args.firstName,
				lastName: args.lastName,
				email: args.email,
				password: bcrypt.hashSync(args.password, 10),
				confirmPassword: bcrypt.hashSync(args.confirmPassword, 10),
				roleId: "5de06eaaa598721e37bafb7f"
			});

			console.log(newUser);

			return newUser.save();
		},

		createVenue: (_, args) => {
			console.log(args);

			let newVenue = new Venue({
				name: args.name,
				description: args.description,
				availabilityId: args.availabilityId
			});

			console.log(newVenue);

			return newVenue.save();
			// }
		},

		// update
		updateBooking: (_, args) => {
			console.log(args);
			let condition = { _id: args.id };
			let updates = {
				startTime: args.startTime,
				endTime: args.endTime,
				statusId: args.statusId,
				venueId: args.venueId,
				userId: args.userId
			};
			// console.log(Team.findOneAndUpdate(condition, updates))
			return Booking.findOneAndUpdate(condition, updates);
		},

		updateVenue: (_, args) => {
			console.log(args);

			let condition = { _id: args.id };
			let updates = {
				name: args.name,
				description: args.description,
				availabilityId: args.availabilityId
			};

			return Venue.findOneAndUpdate(condition, updates);
		},

		updateUser: (_, args) => {
			console.log(args);

			let condition = { _id: args.id };
			let updates = {
				firstName: args.firstName,
				lastName: args.lastName,
				email: args.email,
				password: bcrypt.hashSync(args.password, 10),
				confirmPassword: bcrypt.hashSync(args.confirmPassword, 10)
			};

			return User.findOneAndUpdate(condition, updates);
		},

		deleteUser: (_, args) => {
			console.log(args.id);
			let condition = { _id: args.id };
			return User.findByIdAndDelete(condition, (err, user) => {
				console.log(user);
				if (err || !user) {
					console.log("delete failed, no user found");
					return false;
				}

				console.log("user deleted");
				return true;
			});
		},

		deleteVenue: (_, args) => {
			console.log(args.id);
			let condition = { _id: args.id };
			return Venue.findByIdAndDelete(condition, (err, venue) => {
				console.log(venue);
				if (err || !venue) {
					console.log("delete failed, no venue found");
					return false;
				}

				console.log("venue deleted");
				return true;
			});
		},

		deleteBooking: (_, args) => {
			console.log(args.id);
			let condition = { _id: args.id };
			return Booking.findByIdAndDelete(condition, (err, booking) => {
				if (err || !booking) {
					console.log("delete failed, no booking found");
					return false;
				}

				console.log("booking deleted");
				return true;
			});
		},

		logInUser: (_, args) => {
			console.log("trying to log in");
			console.log(args);

			//
			return User.findOne({ email: args.email }).then(user => {
				console.log(user.password);
				console.log(args.password);
				console.log(user);

				if (user == null) {
					console.log("user not found");
					return null;
				}

				// syntax : bcrypt.compareSync(plain_pass(args.passwrd), hashed password(member.password))

				// create a variable called hashedPassword and assign the value of the result of bcrypt.comparesync
				let hashedPassword = bcrypt.compareSync(
					args.password,
					user.password
				);

				if (!hashedPassword) {
					console.log("password is incorrect");
				} else {
					console.log("successful login!");
					return user;

					// const Nexmo = require("nexmo");

					// const nexmo = new Nexmo({
					// 	apiKey: "fe4e199c",
					// 	apiSecret: "heRmhQpRW485XxUJ"
					// });

					// const from = "Nexmo";
					// const to = "639260658187";
					// const text = "Go go go.";

					// nexmo.message.sendSms(from, to, text);
				}
			});
		}
	},

	// custom type object
	// custom  type resolve
	UserType: {
		// declare a resolver for the tasks field inside TeamType
		roles: (parent, args) => {
			console.log(parent.roleId);
			return Role.find({ _id: parent.roleId });
		}
	},

	BookingType: {
		statuses: (parent, args) => {
			console.log(parent);
			// collection.find(id nung collection na kukunin : collection type id na icocnnect)
			return Status.find({ _id: parent.statusId });
		},
		venues: (parent, args) => {
			return Venue.find({ _id: parent.venueId });
		},
		users: (parent, args) => {
			console.log(parent);
			return User.find({ _id: parent.userId });
		}
	},

	VenueType: {
		availabilities: (parent, args) => {
			console.log(parent.id);
			return Availability.find({ _id: parent.availabilityId });
		}
	}
};

// create an instance of the apollo server
// in the most basic sense, the ApolloServer can be started
// by passing schema type definitions(typeDefs) and the
// resolvers responsible for fetching the data for the declared requests/queries

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
